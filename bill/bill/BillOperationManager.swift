//
//  BillOperationManager.swift
//  bill
//
//  Created by RongJI on 2021/1/19.
//

import UIKit
import LeanCloud
import SwiftyJSON

class BillOperationManager: NSObject {
    
    static let instance = BillOperationManager()
    let className = "Bill"
    var billModel : BillListModel?

    var billTypeArr : NSArray? {
        get {
            let arr = UserDefaults.standard.array(forKey: "typeArr") as NSArray?
            if (arr != nil) {
                return arr
            }
            return []
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "typeArr")
        }
    }
    
    private override init() {
        super.init()
        if billTypeArr?.count == 0 {
            billTypeArr = ["餐饮食品", "交通路费", "日常用品", "服装首饰", "学习教育", "烟酒消费", "房租水电", "网上购物", "运动健身", "电子产品", "化妆护理", "医疗体检", "游戏娱乐", "外出旅游", "油费维护", "慈善捐赠", "其他支出"]
        }
    }
    
    func saveBillInfo (){
        do {
            // 构建对象
            var bill : LCObject!

            if ((billModel?.objectId) != nil) {
                
                bill = LCObject(className: className, objectId:(billModel?.objectId)!)
            }else {
                bill = LCObject(className: className)
            }
            
            // 为属性赋值
            try bill.set("type", value: billModel?.billType)
            try bill.set("time", value: billModel?.billTime)
            try bill.set("money", value: billModel?.billMoney)
            try bill.set("detail", value: billModel?.billDetail)
            
            print(JSON(billModel as Any))
            
            bill.save { (result) in
                switch result {
                case .success:
                  
                    break
                case .failure(error: let error):
                    print(error)
                }
            }
        } catch {
            print(error)
        }
    }

    func getBillsInfo( index:Int, _ closure: @escaping([LCObject]) -> Void) {
        let query = LCQuery(className: className)
        query.limit = 10
        query.skip = query.limit! * index
        _ = query.find { result in
            switch result {
            case .success(objects: let bills):
                closure(bills)
                break
            case .failure(error: let error):
                print(error)
            }
        }
    }
    
    func getBillsInfoByKeynameAndKey(  param :Dictionary<String, Any>, index:Int , _ closure: @escaping([LCObject]) -> Void) {
        let query = LCQuery(className: className)
        query.limit = 10
        query.skip = query.limit! * index
        let keyArr = param["key"] as! NSArray
        let valueArr = param["value"] as! NSArray
        if keyArr.count > 0 {
            for i in 0...keyArr.count-1 {
                let key = keyArr[i] as! String
                let value = valueArr[i] as! String
                query.whereKey( key, .matchedSubstring(value))
            }
        }
        
        _ = query.find { result in
            switch result {
            case .success(objects: let bills):
                closure(bills)
                break
            case .failure(error: let error):
                print(error)
            }
        }
    }
    
    func getBillsInfoByKey( key:String , _ closure: @escaping([LCObject]) -> Void) {
        let query = LCQuery(className: className)
        query.limit = 1000
        query.whereKey("type", .equalTo(key))
        _ = query.find { result in
            switch result {
            case .success(objects: let bills):
                closure(bills)
                break
            case .failure(error: let error):
                closure([])
                print(error)
            }
        }
    }
    
    static func getString( key:String , bill:LCObject ) -> String {
        
        let title:LCString = bill.get(key) as! LCString
        if key == "objectId" {
            return (bill.objectId?.lcString.value)!
        }
        return title.value
    }

}
