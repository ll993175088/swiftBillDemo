//
//  LoginViewController.swift
//  bill
//
//  Created by RongJI on 2021/1/22.
//

import UIKit

class LoginViewController: UIViewController {

    var phoneTF : UITextField = {
        let tf = UITextField.init()
        return tf
    }()
    var passWordTF : UITextField = {
        let tf = UITextField.init()
        return tf
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "用户登录"
        let phoneL = UILabel.init()
        phoneL.text = "手机号"
        phoneL.font = .systemFont(ofSize: 18)
        view.addSubview(phoneL)
        phoneL.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(100)
            make.left.equalToSuperview().offset(40)
        }
        
        phoneTF = UITextField.init()
        phoneTF.placeholder = "请输入手机号"
        phoneTF.font = .systemFont(ofSize: 18)
        view.addSubview(phoneTF)
        phoneTF.snp.makeConstraints { (make) in
            make.top.equalTo(phoneL)
            make.left.equalTo(phoneL.snp.right).offset(10)
        }
        
        let passWordL = UILabel.init()
        passWordL.text = "密码"
        passWordL.font = .systemFont(ofSize: 18)
        view.addSubview(passWordL)
        passWordL.snp.makeConstraints { (make) in
            make.top.equalTo(phoneL.snp.bottom).offset(50)
            make.left.equalToSuperview().offset(40)
        }
        
        passWordTF = UITextField.init()
        passWordTF.placeholder = "请输入密码"
        passWordTF.font = .systemFont(ofSize: 18)
        view.addSubview(passWordTF)
        passWordTF.snp.makeConstraints { (make) in
            make.top.equalTo(passWordL)
            make.left.equalTo(phoneTF)
        }
        
        let loginBtn = UIButton.init(type: .custom)
        loginBtn.setTitle("登录", for: .normal)
        loginBtn.titleLabel?.font = .systemFont(ofSize: 20)
        view.addSubview(loginBtn)
        loginBtn.addTarget(self, action: #selector(loginAction), for: .touchUpInside)
        loginBtn.snp.makeConstraints { (make) in
            make.top.equalTo(passWordL).offset(60)
            make.left.equalToSuperview().offset(40)
        }
        
        let registBtn = UIButton.init(type: .custom)
        registBtn.setTitle("注册", for: .normal)
        registBtn.titleLabel?.font = .systemFont(ofSize: 20)
        view.addSubview(registBtn)
        registBtn.addTarget(self, action: #selector(registAction), for: .touchUpInside)
        registBtn.snp.makeConstraints { (make) in
            make.top.equalTo(passWordL).offset(60)
            make.right.equalToSuperview().offset(-40)
        }
    }
    
    @objc func loginAction (){
        phoneTF.endEditing(true)
        passWordTF.endEditing(true)
        
        UserCommonManager.login(phone: phoneTF.text!, passWord: passWordTF.text!) {
            self.navigationController?.popViewController(animated: true)
        }
    }
     
    @objc func registAction (){
        phoneTF.endEditing(true)
        passWordTF.endEditing(true)
       
        UserCommonManager.regist(phone: phoneTF.text!, passWord: passWordTF.text!) {
            
        }
    }
    
  
}
