//
//  BillSettingViewController.swift
//  bill
//
//  Created by RongJI on 2021/1/22.
//

import UIKit

class BillSettingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    var tableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.tableView = UITableView(frame:self.view.frame, style: .plain)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(self.tableView)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserCommonManager.instance.phoneNum!.isEmpty {
            let vc = LoginViewController.init()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "退出登录"
                break
            case 1:
                cell.textLabel?.text = "清除数据"
                break
            default:
                break
            }
        }
        
        if indexPath.section == 1 {
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "添加类别"
                break
            case 1:
                cell.textLabel?.text = "删除类别"
                break
            default:
                break
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                UserCommonManager.loginOut {
                    let vc = LoginViewController.init()
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                break
            case 1:
                let alertController = UIAlertController.init(title: "提示", message: "是否清除所有数据", preferredStyle: UIAlertController.Style.actionSheet)
                let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
                let okAction = UIAlertAction(title: "好的", style: .default, handler: {
                    action in
                    print("点击了确定")
                })
                alertController.addAction(cancelAction)
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                break
            default:
                break
            }
        }
        if indexPath.section == 1 {
            switch indexPath.row {
            case 0:
                let alertController = UIAlertController(title: "添加类别",
                                         message: "请输入要添加的类别", preferredStyle: .alert)
                     alertController.addTextField {
                         (textField: UITextField!) -> Void in
                         textField.placeholder = "类别名称"
                     }
                    
                     let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
                     let okAction = UIAlertAction(title: "好的", style: .default, handler: {
                         action in

                        let type = alertController.textFields!.first!
                        let mutArr = NSMutableArray.init(array: BillOperationManager.instance.billTypeArr!)
                        mutArr.add(type.text!)
                        BillOperationManager.instance.billTypeArr = mutArr
                     })
                     alertController.addAction(cancelAction)
                     alertController.addAction(okAction)
                     self.present(alertController, animated: true, completion: nil)
                break
            case 1:
                let vc = TypeDeleteViewController.init()
                self.navigationController?.pushViewController(vc, animated: true)
                break
            default:
                break
            }
        }
    }
    
    
}


