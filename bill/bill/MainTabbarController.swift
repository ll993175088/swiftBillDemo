//
//  MainTabbarController.swift
//  bill
//
//  Created by RongJI on 2021/1/12.
//

import UIKit

class MainTabbarController: UITabBarController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let imgs = ["明细","统计","设置"]
        let jizhang = BillListViewController()
        let tongji = BillChartViewController()
        let shezhi = BillSettingViewController()
        let vcs = [jizhang,tongji,shezhi]
        for i in 0..<imgs.count {
            let vc = vcs[i]
            let img = imgs[i]
            self.addChildController(title: img, vc: vc)
        }
    }
    
    func addChildController(title:String, vc:UIViewController) -> () {
        
        vc.tabBarItem.image = UIImage(named: title)
        vc.title = title
        let nav = BaseNavigationController(rootViewController: vc)
        self.addChild(nav)
    }


}
