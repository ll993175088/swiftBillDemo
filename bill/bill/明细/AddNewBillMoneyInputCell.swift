//
//  AddNewBillMoneyInputCell.swift
//  bill
//
//  Created by RongJI on 2021/1/13.
//


import UIKit
import SnapKit

class AddNewBillMoneyInputCell: UITableViewCell {

    var moneyTF : UITextField!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style:style,reuseIdentifier:reuseIdentifier)
        self.selectionStyle = .none
        let label = UILabel()
        self.contentView.addSubview(label)
        label.text = "金额"
        label.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.centerY.equalToSuperview()
        }
        
        
        self.moneyTF = UITextField()
        self.contentView.addSubview(self.moneyTF)
        self.moneyTF.placeholder = "输入金额"
        self.moneyTF.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-10)
            make.centerY.equalToSuperview()
        }
        let keyboad = AddNewBillCalculateInputView(frame: CGRect(x: 0, y: 0, width: ScreenInfo.Width, height: 160))
        keyboad.inputV = self.moneyTF
        self.moneyTF.inputView = keyboad
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
