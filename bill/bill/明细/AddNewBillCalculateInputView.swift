//
//  AddNewBillCalculateInputView.swift
//  bill
//
//  Created by RongJI on 2021/1/15.
//

import UIKit

class AddNewBillCalculateInputView: UIView {
    
    enum CalculterOperation {
        
        case add,minus,mutiply,div
    }
    
    var showLabel: UILabel?
    var firstOperatNumber :String? //第一个操作数
    var secondOperatNumber :String? //第二个操作数
    var currentOperation: CalculterOperation? //定义枚举类型，进行加减乘法的计算
    var tempConvertString = ""

    var inputV : UITextField?
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.buildCalculateBtn()
        self.buildNumBtn()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildCalculateBtn() {
        let firstLineArray = ["＋","－","×","÷"]
        let btnWidth = 60
        let btnHeight = 40
        var top = 0
        for index in 0..<firstLineArray.count {
            
            let columnOneButton = UIButton(frame: CGRect(x: 0, y: top, width: btnWidth, height: btnHeight))
            columnOneButton.backgroundColor = .lightGray
            columnOneButton.setTitle(firstLineArray[index], for: .normal)
            columnOneButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 40)
            columnOneButton.setTitleColor(.black, for: .normal)
            columnOneButton.addTarget(self, action: #selector(calculateAction(btn:)), for: .touchUpInside)
            columnOneButton.layer.borderWidth = 1
            columnOneButton.layer.borderColor = UIColor.black.cgColor
            
            self.addSubview(columnOneButton)
            top += btnHeight
        }
    }
    
    func buildNumBtn() {
        let firstLineArray = ["1","2","3","4","5","6","7","8","9",".","0","←"]
        let btnWidth = (ScreenInfo.Width - 60)/3
        let btnHeight = 40
        for index in 0..<firstLineArray.count {
            
            let columnOneButton = UIButton(frame: CGRect(x: 60 + index%3*Int(btnWidth) , y: index/3*(btnHeight), width: Int(btnWidth), height: btnHeight))
            columnOneButton.backgroundColor = .white
            if firstLineArray[index] == "." || firstLineArray[index] == "←"{
                columnOneButton.backgroundColor = .lightGray
            }
            columnOneButton.setTitle(firstLineArray[index], for: .normal)
            columnOneButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
            columnOneButton.setTitleColor(.black, for: .normal)
            columnOneButton.addTarget(self, action: #selector(numberAction(btn:)), for: .touchUpInside)
            columnOneButton.layer.borderWidth = 1
            columnOneButton.layer.borderColor = UIColor.black.cgColor
            
            self.addSubview(columnOneButton)
        }
    }
    
    @objc func calculateAction (btn : UIButton) {
        
        if self.firstOperatNumber!.count > 0 {
            
            if self.secondOperatNumber!.count > 0 {
                
                
                
            }else {
                
            }
        }
    }
    
    @objc func numberAction (btn : UIButton) {
        
        if btn.titleLabel?.text == "←" {
            if (self.inputV?.text!.count)! > 0 {
                self.inputV?.text?.removeLast(1)
            }
        }else {
            self.inputV?.text! += (btn.titleLabel?.text)!
        }
        
    }
    
    //计算结果的方法
    func calcuaterResult(firstNum firstNumber:String,secondNum secondNumber:String,opt operation:CalculterOperation) -> String {
        
        var resultstring = "0"
        var firstValue = 0
        var secondValue = 0
        
        if firstNumber.contains(".") || secondNumber.contains(".") { //此处是两个float数相加
            
            
        }else{
            
           firstValue = Int(firstNumber)!
           secondValue = Int(secondNumber)!
            
        }
        
        switch operation {
        case .add:
            
            let addResult:Int = Int(firstValue) + Int(secondValue)
            resultstring = "\(addResult)"
            
            break
            
        case .minus:
            
            let addResult:Int = Int(firstValue) - Int(secondValue)
            resultstring = "\(addResult)"
            
            break
       case .mutiply:
               
               let addResult:Int = Int(firstValue) * Int(secondValue)
               resultstring = "\(addResult)"
               
               break
        case .div:
            let addResult:Int = Int(firstValue) / Int(secondValue)
            resultstring = "\(addResult)"
            break
        }
        
        return resultstring
        
    }
    
}
