//
//  BillListViewController.swift
//  bill
//
//  Created by RongJI on 2021/1/19.
//

import UIKit
import LeanCloud
import PullToRefreshKit

class BillListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var addBtn : UIButton = {
        let btn = UIButton.init()
        btn.setTitle("新增", for: .normal)
        return btn
    }()
    
    var dataArr : NSMutableArray = {
        let arr = NSMutableArray.init()
        return arr
    }()
    
    var index : Int = 0
    var param : [String : NSArray] = ["key":[],"value":[]]
    
    var tableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        self.tableView = UITableView(frame:self.view.frame, style: .plain)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(BillListCell.self, forCellReuseIdentifier: "cell")
//        let headView = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 80))
//        headView.text = "head"
//        self.tableView.tableHeaderView = headView
        self.view.addSubview(self.tableView)
        self.tableView.configRefreshHeader(container: self) {
            self.dataArr.removeAllObjects()
            self.index = 0
            self.getDate()
        }
        self.tableView.configRefreshFooter(container: self) {
            self.index += 1
            self.getDate()
        }
        
        
        self.addBtn.frame = CGRect(x: ScreenInfo.Width - 50, y: ScreenInfo.Height - 100, width: 40, height: 40)
        self.addBtn.layer.cornerRadius = 20
        self.addBtn.backgroundColor = .orange
        self.addBtn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        self.addBtn.addTarget(self, action: #selector(addNewBill), for: .touchUpInside)
        self.view.addSubview(self.addBtn)
        
        let topView = UIView.init(frame: CGRect(x: 0, y: ScreenInfo.navigationHeight, width: ScreenInfo.Width, height: 80))
        topView.backgroundColor = .darkGray
        self.view.addSubview(topView)
        
        let typeBtn = UIButton.init(type: .custom)
        typeBtn.frame = CGRect(x: 20, y: 0, width: ScreenInfo.Width - 40, height: 40)
        typeBtn.setTitleColor(.blue, for: .normal)
        typeBtn.setTitle("收支类型", for: .normal)
        topView.addSubview(typeBtn)
        typeBtn.addTarget(self, action: #selector(getDataByType(btn:)), for: .touchUpInside)
        
        let timeBtn = UIButton.init(type: .custom)
        timeBtn.frame = CGRect(x: 20, y: 40, width: ScreenInfo.Width - 40, height: 40)
        timeBtn.setTitleColor(.blue, for: .normal)
        timeBtn.setTitle("选择时间", for: .normal)
        topView.addSubview(timeBtn)
        timeBtn.addTarget(self, action: #selector(getDataByTime(btn:)), for: .touchUpInside)
        
        
        self.tableView.frame = CGRect(x: 0, y: ScreenInfo.navigationHeight + 80, width: ScreenInfo.Width, height: ScreenInfo.Height - 80 -  ScreenInfo.navigationHeight)
        
        self.getDate()
    }

    @objc func getDataByType ( btn:UIButton ) {
        let typePicker = BillTypePickerViewController()
        
        /// 回调显示方法
        typePicker.typeBack = { [weak self] type in
            btn.setTitle(type, for: .normal)
            self?.param = ["key":["type"],"value":[type]]
            self?.dataArr.removeAllObjects()
            self?.index = 0
            self?.getDate()
        }
        UIViewController.current().present(typePicker, animated: true) {
            
        }
    }
    
    @objc func getDataByTime ( btn:UIButton ) {
        let dataPicker = DataPickerViewController()
        dataPicker.type = .month
        /// 回调显示方法
        dataPicker.backDate = { [weak self] dateString in
            btn.setTitle(dateString, for: .normal)
            self?.param = ["key":["time"],"value":[dateString]]
            self?.dataArr.removeAllObjects()
            self?.index = 0
            self?.getDate()
        }
        dataPicker.picker.reloadAllComponents()
        /// 弹出时日期滚动到当前日期效果
        UIViewController.current().present(dataPicker, animated: true) {
        }
    }
    
    func getDate() {
        
        BillOperationManager.instance.getBillsInfoByKeynameAndKey(param: self.param, index: self.index, { (bills) in
            
            for bill in bills {
                let model = BillListModel.init()
                model.billTime = BillOperationManager.getString(key: "time", bill: bill)
                model.billType = BillOperationManager.getString(key: "type", bill: bill)
                model.billDetail = BillOperationManager.getString(key: "detail", bill: bill)
                model.billMoney = BillOperationManager.getString(key: "money", bill: bill)
                model.objectId = BillOperationManager.getString(key: "objectId", bill: bill)
                self.dataArr.add(model)
            }
            self.tableView.switchRefreshHeader(to: .normal(RefreshResult.success, 0.2))
            self.tableView.switchRefreshFooter(to: .normal)
            if bills.count < 10 {
                self.tableView.switchRefreshFooter(to: .noMoreData)
            }
            self.tableView.reloadData()
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArr.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BillListCell
        let model = self.dataArr[indexPath.row] as! BillListModel

        cell.typeL.text = model.billType
        cell.describL.text = model.billDetail
        cell.moneyL.text = model.billMoney
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = AddNewBillViewController()
        let model = self.dataArr[indexPath.row] as! BillListModel
        vc.model = model
        vc.updateBill = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @objc func addNewBill() {
        let VC = AddNewBillViewController.init()
        self.navigationController?.pushViewController(VC, animated: true)
    }
}


