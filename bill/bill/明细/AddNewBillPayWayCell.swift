//
//  AddNewBillPayWayCell.swift
//  bill
//
//  Created by RongJI on 2021/1/13.
//

import UIKit
class AddNewBillPayWayCell: UITableViewCell {

    
    var typeBtn : UIButton = {
        let btn = UIButton()

        return btn
    }()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style:style,reuseIdentifier:reuseIdentifier)
        self.selectionStyle = .none

        let titleL = UILabel()
        titleL.text = "收支类型"
        self.contentView.addSubview(titleL)
        titleL.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.centerY.equalToSuperview()
        }
        
        self.contentView.addSubview(self.typeBtn)
        self.typeBtn.setTitle( "选择收支类型", for: .normal)
        self.typeBtn.setTitleColor(UIColor.blue, for: .normal)
        self.typeBtn.addTarget(self, action: #selector(btnAction(btn:)), for: .touchUpInside)
        self.typeBtn.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-10)
            make.centerY.equalToSuperview()
            
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    @objc func btnAction (btn:UIButton){
        let typePicker = BillTypePickerViewController()
        
        /// 回调显示方法
        typePicker.typeBack = { [weak self] type in
            btn.setTitle(type, for: .normal)
            BillOperationManager.instance.billModel?.billType = type
        }
        /// 弹出时日期滚动到当前日期效果
        UIViewController.current().present(typePicker, animated: true) {
            
        }
    }
}
