//
//  AddNewBillTimeCell.swift
//  bill
//
//  Created by RongJI on 2021/1/13.
//

import UIKit

class AddNewBillTimeCell: UITableViewCell {
    
    private var currentDateCom: DateComponents = Calendar.current.dateComponents([.year, .month, .day], from: Date())
    var timeL : UILabel = {
        let label = UILabel()
        label.text = "选择时间"
        label.textColor = UIColor.blue
        
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style:style,reuseIdentifier:reuseIdentifier)
        
        self.selectionStyle = .none
        
        let titleL = UILabel()
        titleL.text = "收支时间"
        self.contentView.addSubview(titleL)
        titleL.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.centerY.equalToSuperview()
        }
        
        self.contentView.addSubview(self.timeL)
        self.timeL.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-10)
            make.centerY.equalToSuperview()
            make.height.equalTo(40)
        }
        
        let btn = UIButton()
        self.contentView.addSubview(btn)
        btn.addTarget(self, action: #selector(btnAction(btn:)), for: .touchUpInside)
        btn.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalToSuperview()
            make.height.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    @objc func btnAction (btn:UIButton){
        let dataPicker = DataPickerViewController()
        
        /// 回调显示方法
        dataPicker.backDate = { [weak self] dateString in
            self?.timeL.text = dateString
            BillOperationManager.instance.billModel?.billTime = dateString
        }
        dataPicker.picker.reloadAllComponents()
        /// 弹出时日期滚动到当前日期效果
        UIViewController.current().present(dataPicker, animated: true) {
            dataPicker.picker.selectRow(0, inComponent: 0, animated: true)
            dataPicker.picker.selectRow((self.currentDateCom.month!) - 1, inComponent: 1, animated:   true)
            dataPicker.picker.selectRow((self.currentDateCom.day!) - 1, inComponent: 2, animated: true)
        }
    }
    
}
