//
//  BillListCell.swift
//  bill
//
//  Created by RongJI on 2021/1/18.
//

import UIKit

class BillListModel: NSObject {
    
    var billType : String?
    var billTime : String?
    var billMoney : String?
    var billDetail : String?
    var objectId : String?

}

class BillListCell: UITableViewCell {
    
    var typeL : UILabel = {
        let label = UILabel()
        label.textColor = .blue
        label.font = .boldSystemFont(ofSize: 18)
        return label
    }()
    var moneyL : UILabel = {
        let label = UILabel()
        label.textColor = .red
        label.font = .boldSystemFont(ofSize: 20)
        return label
    }()
    var describL : UILabel = {
        let label = UILabel()
        label.textColor = .lightGray
        label.font = .systemFont(ofSize: 14)
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style:style,reuseIdentifier:reuseIdentifier)
        
        self.selectionStyle = .none
        
        self.contentView.addSubview(self.typeL)
        self.typeL.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.top.equalToSuperview().offset(10)
        }
        self.contentView.addSubview(self.moneyL)
        self.moneyL.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.top.equalTo(self.typeL.snp.bottom).offset(5)
        }
        self.contentView.addSubview(self.describL)
        self.describL.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.top.equalTo(self.moneyL.snp.bottom).offset(5)
        }
        

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
