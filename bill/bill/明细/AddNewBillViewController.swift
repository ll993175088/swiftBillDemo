//
//  AddNewBillViewController.swift
//  bill
//
//  Created by RongJI on 2021/1/13.
//

import UIKit

class AddNewBillViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var tableView : UITableView!
    var inputCell : AddNewBillMoneyInputCell?
    var model : BillListModel?
    var updateBill : Bool = {
        return false
    }()
    
    var detailTF : UITextField = {
        return UITextField()
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        BillOperationManager.instance.billModel = BillListModel()
        if self.updateBill {
            BillOperationManager.instance.billModel = self.model
        }
        
        self.tableView = UITableView(frame:self.view.frame, style: .plain)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(AddNewBillTimeCell.self, forCellReuseIdentifier: "AddNewBillTimeCell")
        self.tableView.register(AddNewBillPayWayCell.self, forCellReuseIdentifier: "AddNewBillPayWayCell")
        self.tableView.register(AddNewBillMoneyInputCell.self, forCellReuseIdentifier: "AddNewBillMoneyInputCell")
        self.view.addSubview(self.tableView)

        let headView = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 80))
        headView.text = "填写收支"
        headView.textAlignment = .center
        self.tableView.tableHeaderView = headView
        
        self.detailTF = UITextField(frame: CGRect(x: 10, y: 0, width: self.view.bounds.width - 20, height: 80))
        self.detailTF.placeholder = "详细描述（选填）"
        self.detailTF.text = self.model?.billDetail
        self.tableView.tableFooterView = self.detailTF

        let rightItem = UIBarButtonItem(title: "保存", style: .plain, target: self, action: #selector(rightItemAction))
        self.navigationItem.rightBarButtonItem = rightItem
    }
    
    @objc func rightItemAction() {
        
        BillOperationManager.instance.billModel?.billMoney = self.inputCell?.moneyTF.text
        BillOperationManager.instance.billModel?.billDetail = self.detailTF.text
        BillOperationManager.instance.saveBillInfo()
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewBillMoneyInputCell", for: indexPath)
            self.inputCell = (cell as! AddNewBillMoneyInputCell)
            if self.updateBill {
                self.inputCell?.moneyTF.text = self.model?.billMoney
            }
            return cell
        }
        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewBillPayWayCell", for: indexPath) as! AddNewBillPayWayCell
            if self.updateBill {
                cell.typeBtn.setTitle(self.model?.billType, for: .normal)
            }
            return cell
        }
        if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewBillTimeCell", for: indexPath) as! AddNewBillTimeCell
            if self.updateBill {
                cell.timeL.text = self.model?.billTime
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
}


